Usage: run [--help] ClassName 'string'
	--help	 list this help message
	
	<ClassName>  a valid class name that has a #process: method
	'string' a Smalltalk string passed to the  #process: method of <ClassName> 

Documentation:
A CommandLineHandler that looks up  a ClassName in the image and then calls its  #process: method passing the 'string' as an argument. After process'ing, the image  quits.  The  ClassName object must write to stdout to return results back to any calling process.

NOTE: Be careful of OS command quoting, you may have to escape ' characters e.g.:

./pharo Pharo.image  process MyClass   '\''{ "hello" : "world" }'\'''

IMPORTANT: Don't try to save the image in  process:  by using something like 'Smalltalk snapshot: true andSave: true'!