internal
localeTime: isoString
	| localeParts country userZone |
	localeParts := $- split: isoString.
	country := localeParts last.
	
	userZone := (ZTimezone zoneTab detect: [  :v | v last = country ] ifNone: [ { ZTimezone gmt id. '' } ]) first.
	
	^ ((ZTimezone id: userZone) gmtToLocal: ZTimestamp now) asTime