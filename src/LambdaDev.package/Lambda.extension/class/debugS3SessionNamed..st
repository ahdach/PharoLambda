*LambdaDev
debugS3SessionNamed: aName
	"Open a debugger on a saved lambda session.
	NOTE: Assumes that AWSS3Config has been initialized e.g.
	
	AWSS3Config default 	
		accessKeyId:'key';
		secretKey:'secret';
		regionName: 'region';
		at: #defaultBucket put: 'bucket'.
	"
	| s3 bucket gzip materializedContext sessionName |
	
	s3 := AWSS3Zip new.
	bucket:= s3 bucketNamed: (s3 awsConfig at: #defaultBucket).
	gzip := GZipReadStream on: (bucket getObject: aName).

	materializedContext := (FLMaterializer newDefault
      materializeFrom: gzip) root.

	sessionName := 'debug saved Fuel context: {1}' format: {aName}.
	
	GTGenericStackDebugger openOn: 
		((Process 
			forContext: materializedContext 
			priority: Processor userInterruptPriority)
			newDebugSessionNamed: sessionName startedAt: 	materializedContext).